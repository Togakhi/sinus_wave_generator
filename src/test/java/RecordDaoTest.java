import app.dao.RecordDao;
import app.model.Player;
import app.model.Record;
import app.tools.tools;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;


public class RecordDaoTest {

    /**
     * Le pattern de la date et du temps pour générer des noms
     */
    private String pattern = "dd-MM-yyyy HH-mm-ss";

    /**
     * L'entity Manager Factory
     */
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("signal");
    ;

    /**
     * L'entity manager
     */
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    ;


    /**
     * Le dao des record
     */
    private final RecordDao recordDao = new RecordDao(entityManager);

    /**
     * Liste des records
     */
    private final List <Record> records = new ArrayList <>();

    /**
     * taille de la liste
     */
    private int taille;

    /**
     * nom du premier record
     */
    private String name;


    /*
     *  Méthode pour init les données avant tout les test
     */
    @Before
    public void init() throws InterruptedException {
        //EntityManagerFactory


        //EntityManager


        //frequence et amplitude
        double freq = 120.0;
        double ampl = 20;

        //récupération d'un son pour les test
        double a[] = Player.note(freq, 5, ampl);

        //Conversion en bytes
        byte[] bytes = tools.DoublestoBytes(a);

        Record record1 = new Record(freq, ampl);
        Record record2 = new Record(freq, ampl);
        Record record3 = new Record(freq, ampl);
        Record record4 = new Record(freq, ampl);

        //Init audios
        record1.setAudio(bytes);
        record2.setAudio(bytes);
        record3.setAudio(bytes);
        record4.setAudio(bytes);

        //Init nom
        name = tools.DateToString(pattern);

        //Init name du 1er record
        record1.setName(name);
        Thread.sleep(1000);

        //Init name du 1er record
        record2.setName(tools.DateToString(pattern));
        Thread.sleep(1000);

        //Init name du 1er record
        record3.setName(tools.DateToString(pattern));
        Thread.sleep(1000);

        //Init name du 1er record
        record4.setName(tools.DateToString(pattern));
        Thread.sleep(1000);

        entityManager.getTransaction().begin();
        entityManager.persist(record1);
        entityManager.persist(record2);
        entityManager.persist(record3);
        entityManager.persist(record4);
        entityManager.getTransaction().commit();

        records.add(record1);
        records.add(record2);
        records.add(record3);
        records.add(record4);

        taille = recordDao.getAll().size();

    }

    /*
     *  Test récupération d'un record
     */
    @Test
    public void testGetRecordAndPlay() {
        Optional <Record> record = recordDao.getObjectById(name);
        assertEquals(name, record.get().getName());
        Player.play(tools.BytestoDoubles(record.get().getAudio()));
    }

    /*
     *  Test récupération de tous les records
     */
    @Test
    public void testGetAllRecord() {
        List <Record> articleList = new ArrayList <>(recordDao.getAll());
        assertEquals(taille, articleList.size());
    }

    /*
     *  Test Ajout d'un record
     */
    @Test
    public void testAddRecord() throws IOException {

        Record recordToAdd = new Record();

        recordToAdd.setName(tools.DateToString(pattern));

        //récupération d'un son pour les test
        double a[] = Player.note(120.0, 5, 20);

        //Conversion en bytes
        byte[] bytes = tools.DoublestoBytes(a);

        //Ajout de l'audio
        recordToAdd.setAudio(bytes);
        recordToAdd.setFrequence(120.0);
        recordToAdd.setAmplitude(20);

        //Ajout du record
        recordDao.add(recordToAdd);

        List <Record> articleList = new ArrayList <>(recordDao.getAll());
        assertEquals(taille, articleList.size());

        entityManager.getTransaction().begin();
        entityManager.remove(recordToAdd);
        entityManager.getTransaction().commit();

    }


    /*
     *  Clean up de la base de données
     */
    @After
    public void cleanOut() {
        if (!records.isEmpty()) {
            entityManager.getTransaction().begin();
            for (Record art : records
            ) {
                entityManager.remove(art);
            }
            entityManager.getTransaction().commit();
        }
    }

}
   