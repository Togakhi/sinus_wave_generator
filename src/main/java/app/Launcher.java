package app;

import app.Controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;

public class Launcher extends Application {


    @Override
    public void start(Stage primaryStage) throws IOException {


        //Initialisation de l'entityManager
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("signal");

        EntityManager entityManager = entityManagerFactory.createEntityManager();



        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main.fxml"));
        Parent root = loader.load();

        MainController mainController = loader.<MainController>getController();
        mainController.setEntityManager(entityManager);
        mainController.init();

        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Home");
        primaryStage.setResizable(false);
        primaryStage.show();

    }
}
