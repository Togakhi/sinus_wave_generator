package app.Controller;

import app.dao.RecordDao;
import app.model.Player;
import app.model.Record;
import app.tools.tools;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.persistence.EntityManager;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainController {



    /**
     * Le pattern de la date et du temps pour générer des noms
     */
    private String pattern = "dd-MM-yyyy HH-mm-ss";

    /**
     * L'entity manager
     */
    private EntityManager entityManager;

    /**
     * Le DAO pour gérer les records
     */
    private RecordDao recordDao;

    /**
     * Les données de la tableView
     */
    private ObservableList<Record> donnees;

    @FXML
    private TextField freqTextField;

    @FXML
    private TextField amplTextField;

    @FXML
    private TableView <Record> SignalTableView;

    @FXML
    private TableColumn <Record, String> nameColumn;

    @FXML
    private TableColumn<Record, Double> freqColumn;

    @FXML
    private TableColumn<Record, Double> amplColumn;


    public void init(){

        //Initialisation du DAO
        recordDao =  new RecordDao(entityManager);

        //Set up des colonnes
        nameColumn.setCellValueFactory(new PropertyValueFactory <Record, String>("Name"));
        freqColumn.setCellValueFactory(new PropertyValueFactory <Record, Double>("Frequence"));
        amplColumn.setCellValueFactory(new PropertyValueFactory <Record, Double>("Amplitude"));

        setDonnees(FXCollections.observableArrayList(recordDao.getAll()));

        SignalTableView.setItems(donnees);
    }


    @FXML
    void generate(ActionEvent event) {


        //récupération des champs
        String frequence = freqTextField.getText();
        String amplitude = amplTextField.getText();

        if(!frequence.isEmpty() && !amplitude.isEmpty()){
            //Initialisation du record à ajouter
            Record recordtoAdd = new Record();

            //Nom du record
            recordtoAdd.setName(tools.DateToString(pattern));

            //Récupération des fréquences/amplitudes
            double ampl = Double.parseDouble(amplitude);
            double freq = Double.parseDouble(frequence);

            //Conversion String to Double
            recordtoAdd.setAmplitude(ampl);
            recordtoAdd.setFrequence(freq);

            //Récupération du son produit
            double[] audio = Player.note(freq,5,ampl);

            recordtoAdd.setAudio(tools.DoublestoBytes(audio));

            //Ajout
            recordDao.add(recordtoAdd);

            //Refresh de la tableview
            donnees.add(recordtoAdd);
            SignalTableView.setItems(donnees);
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setContentText("Un ou plusieurs champs ne sont pas remplis !");
            alert.showAndWait();
        }
    }

    @FXML
    void play(ActionEvent event) {
        if(SignalTableView.getSelectionModel().getSelectedItem() != null){
            String name = SignalTableView.getSelectionModel().getSelectedItem().getName();

            Record recordtoPlay = recordDao.getObjectById(name).get();

            System.out.println(recordtoPlay.getAudio().length);

            Player.play(tools.BytestoDoubles(recordtoPlay.getAudio()));
        }else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setContentText("Le record est introuvable !");
            alert.showAndWait();
        }
    }

    @FXML
    public void save(ActionEvent actionEvent) {
        if(SignalTableView.getSelectionModel().getSelectedItem() != null){
            String name = SignalTableView.getSelectionModel().getSelectedItem().getName();

            Record recordtoPlay = recordDao.getObjectById(name).get();

            Player.save(name+".wav",tools.BytestoDoubles(recordtoPlay.getAudio()));
        }else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setContentText("Le record est introuvable !");
            alert.showAndWait();
        }
    }





    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    public ObservableList <Record> getDonnees() {
        return donnees;
    }
    public void setDonnees(ObservableList<Record> donnees) {
        this.donnees = donnees;
    }


}
