package app.model;

/**
 * Created by Chaairat Tariq on 2020/08/01.
 */

import javax.persistence.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a record in the database.
 *
 */

@Entity
public class Record {
    /**
     * The name of the record.
     */
    @Id
    private String name;
    /**
     * The frequency
     */
    private double frequence;

    /**
     * The amplitude
     */
    private double amplitude;

    /**
     * The audio
     */
    @Lob
    byte[] audio;


    /**
     * Record default builder.
     *
     * Name and path will be empty.
     */
    public Record(){
        name = "";
    }

    /**
     * Constructor with 2 parameters.
     *
     * @param f the frequency of the record
     * @param a the amplitude of the record
     */
    public Record(double f, double a){
        this.frequence = f;
        this.amplitude = a;
    }


    /**

     * Getter for the name of the record.
     *
     * @return the name of the record
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name of the record.
     *
     * @param name the new name for the record
     */
    public void setName( String name ) {
        this.name = name;
    }


    public double getFrequence() {
        return frequence;
    }

    public void setFrequence(double freq) {
        this.frequence = freq;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double ampl) {
        this.amplitude = ampl;
    }

    public byte[] getAudio() {
        return audio;
    }

    public void setAudio(byte[] audio) {
        this.audio = audio;
    }

    /**
     * ToString method
     */
    @Override
    public String toString() { return name;}
}
