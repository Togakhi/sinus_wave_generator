package app.dao;

import app.model.Record;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

public class RecordDao extends DAO<Record> {

    public RecordDao(EntityManager entityManager){
        super(entityManager);
    }

    @Override
    public boolean add(Record element) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(element);
            System.out.println("Add successful !");
            entityManager.getTransaction().commit();

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean modify(Record element) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(element);
            entityManager.getTransaction().commit();
            if (entityManager.contains(element)) {
                System.out.println("Modify successful !");
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public Optional <Record> getObjectById(String id) {
        try{
            return entityManager.createQuery("SELECT record FROM Record record WHERE record.name =:name", Record.class)
                    .setParameter("name", id)
                    .getResultList()
                    .stream()
                    .findFirst();

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Record> getAll() {
        List<Record> candidacies;
        try{
            TypedQuery<Record> query = entityManager.
                    createQuery("SELECT R FROM Record R", Record.class);
            candidacies = query.getResultList();
            return candidacies;
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

}

