package app.dao;

import app.model.Record;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

public abstract class DAO<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    public DAO(EntityManager entityManager){this.entityManager = entityManager;}

    public abstract boolean add(T element);

    public abstract boolean modify(T element);

    //public abstract boolean delete(T element);

    public abstract Optional <T> getObjectById(String id);

    public abstract List<T> getAll();

}
