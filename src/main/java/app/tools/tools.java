package app.tools;

import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class tools {

    /**
     * Le buffer pour lire les données
     */
    private static ByteBuffer byteBuffer;

    /**
     * La longueur d'un record
     */
    private static final int AUDIO_LENGTH = 220501;

    /**
     * Convertis un table de double en tableau de byte
     * @param doubles tableau de double
     * @return byte[] tableau de byte
     */
    public static byte[] DoublestoBytes(double[] doubles){
        byteBuffer = ByteBuffer.allocate(AUDIO_LENGTH*8);
        for (double d : doubles){
            byteBuffer.putDouble(d);
        }

        return byteBuffer.array();
    }

    /**
     * Convertis un table de byte en tableau de double
     * @param bytes tableau de byte
     * @return double[] tableau de double
     */
    public static double[] BytestoDoubles(byte[] bytes){
        byteBuffer = ByteBuffer.wrap(bytes);
        double[] doubles = new double[AUDIO_LENGTH];
        System.out.println(doubles.length);
        for(int i=0 ; i< doubles.length;i++){
            doubles[i]=byteBuffer.getDouble();
        }
        return doubles;
    }


    /**
     * Retourne la date d'aujourd'hui en String
     * @param pattern le pattern de la date
     * @return
     */
    public static String DateToString(String pattern){
        //Format de la date
        DateFormat df = new SimpleDateFormat(pattern);

        //Récupération de la date + heure actuelle
        Date today = Calendar.getInstance().getTime();

        //conversion en string suivant le format
        String todayString = df.format(today);

        return todayString;
    }
}
